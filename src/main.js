import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
Vue.config.productionTip = false

import Vuesax from 'vuesax'
import './slicing.scss'

// import 'vue-scroll-up/dist/style.css'

import VueSplide from '@splidejs/vue-splide';

Vue.use( VueSplide );

var VueScrollTo = require('vue-scrollto');

// You can also pass in the default options
Vue.use(VueScrollTo)


import 'vuesax/dist/vuesax.css' //Vuesax styles
Vue.use(Vuesax, {
  // options here
  theme:{
    colors:{
      primary:'#5b3cc4',
      success:'rgb(23, 201, 100)',
      danger:'rgb(242, 19, 93)',
      warning:'rgb(255, 130, 0)',
      dark:'rgb(36, 33, 69)',
      white: '#ffffff'
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
